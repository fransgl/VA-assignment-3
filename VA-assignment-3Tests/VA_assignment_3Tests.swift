//
//  VA_assignment_3Tests.swift
//  VA-assignment-3Tests
//
//  Created by Frans Glorie on 09/02/2018.
//  Copyright © 2018 Frans Glorie. All rights reserved.
//

import XCTest
@testable import VA_assignment_3

class VA_assignment_3Tests: XCTestCase {
    
    // Test variables
    // 2018-02-11
    let testDate = Date(timeIntervalSince1970: 1518350400)
    var scheduleViewModel: ScheduleViewModel!
    var scheduleModel: ScheduleModel!
    
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        scheduleModel = ScheduleModel(dateBegin: testDate, dateEnd: testDate, frequency: .weekly)
        scheduleViewModel = ScheduleViewModel(model: scheduleModel)
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testDateStringFormatter() {
        
        XCTAssert(testDate.toLocalDateString == "11 February 2018")
        
    }
    
    func testMinimumEndDate() {
        
        scheduleViewModel.frequency = .daily
        XCTAssert(scheduleViewModel.minimumEndDateString == "12 February 2018")
        scheduleViewModel.frequency = .weekly
        XCTAssert(scheduleViewModel.minimumEndDateString == "18 February 2018")
        scheduleViewModel.frequency = .monthly
        XCTAssert(scheduleViewModel.minimumEndDateString == "11 March 2018")
        
    }
    
    func testFrequencyText() {
        
        scheduleViewModel.frequency = .daily
        XCTAssert(scheduleViewModel.frequencyText == "Daily")
        scheduleViewModel.frequency = .weekly
        XCTAssert(scheduleViewModel.frequencyText == "Weekly")

    }
    
    func testFrequencyID() {
        
        scheduleViewModel.frequency = .daily
        XCTAssert(scheduleViewModel.frequencyID == 1)
        scheduleViewModel.frequency = .weekly
        XCTAssert(scheduleViewModel.frequencyID == 2)
        
    }
    
    func testOnceIsSelected() {
        
        scheduleViewModel.frequency = .once
        XCTAssert(scheduleViewModel.onceIsSelected == true)
        scheduleViewModel.frequency = .weekly
        XCTAssert(scheduleViewModel.onceIsSelected == false)
        
    }
    
}
