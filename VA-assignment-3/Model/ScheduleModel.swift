//
//  ScheduleModel.swift
//  VA-assignment-2
//
//  Created by Frans Glorie on 08/02/2018.
//  Copyright © 2018 Frans Glorie. All rights reserved.
//

import Foundation

// Enums and structs
enum ScheduleFrequency: Int {
    case once = 0
    case daily = 1
    case weekly = 2
    case monthly = 3
    
    static var count: Int { return self.monthly.rawValue + 1 }
    
    var description: String {
        switch self {
        case .once: return "Once"
        case .daily: return "Daily"
        case .weekly: return "Weekly"
        case .monthly: return "Monthly"
        }
    }
}

class ScheduleModel {
    
    // Variables
    var dateBegin: Date
    var dateEnd: Date
    var frequency: ScheduleFrequency
    
    // Functions
    init(dateBegin: Date, dateEnd: Date, frequency: ScheduleFrequency) {
        self.dateBegin = dateBegin
        self.dateEnd = dateEnd
        self.frequency = frequency
    }
    
}
