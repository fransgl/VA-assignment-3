//
//  ScheduleTableViewController.swift
//  VA-assignment-3
//
//  Created by Frans Glorie on 10/02/2018.
//  Copyright © 2018 Frans Glorie. All rights reserved.
//

import UIKit

protocol CollapsableTableViewCell {
    var isCollapsable: Bool { get }
    var expandedHeight: CGFloat { get }
    var collapsedHeight: CGFloat { get }
}

class ScheduleTableViewController: UITableViewController,
                                    UIPickerViewDelegate,
UIPickerViewDataSource {

    // Outlets
    @IBOutlet weak var labelBeginDate: UILabel!
    @IBOutlet weak var datePickerBeginDate: UIDatePicker!

    @IBOutlet weak var labelFrequency: UILabel!
    @IBOutlet weak var pickerFrequency: UIPickerView!
 
    @IBOutlet weak var labelEndDate: UILabel!
    @IBOutlet weak var datePickerEndDate: UIDatePicker!
    
    // Actions
    @IBAction func datePickerBeginDateValueChanged(_ sender: UIDatePicker) {
        
        viewModel.beginDateUTC = sender.date
        labelBeginDate.text = viewModel.beginDateString
        
        updateEndDate()
    }
    @IBAction func datePickerEndDateValueChanged(_ sender: UIDatePicker) {
        viewModel.endDateUTC = datePickerEndDate.date
        labelEndDate.text = viewModel.endDateString
    }
    @IBAction func buttonClear(_ sender: UIBarButtonItem) {
        
        viewModel.beginDateUTC = Date()
        viewModel.frequency = .once
        expandedCell = -1
        
        updateViewFromViewModel()
        updateTableviewLayout()
    
    }
    
    // Variables
    var viewModel: ScheduleViewModel = ScheduleViewModel(model: ScheduleModel(dateBegin: Date(), dateEnd: Date(), frequency: .once))
    private var expandedCell: Int = -1
    
    // Functions
    override func viewDidLoad() {
        super.viewDidLoad()

        pickerFrequency.delegate = self
        pickerFrequency.dataSource = self
        
        datePickerBeginDate.minimumDate = Date()
        
    }

    override func viewDidAppear(_ animated: Bool) {

        updateViewFromViewModel()
        updateTableviewLayout()
        
    }

    func updateViewFromViewModel() {
        
        labelBeginDate.text = viewModel.beginDateString
        labelEndDate.text = viewModel.endDateString
        labelFrequency.text = viewModel.frequencyText
        datePickerBeginDate.date = viewModel.beginDateUTC
        datePickerEndDate.date = viewModel.endDateUTC
        pickerFrequency.selectRow(viewModel.frequencyID, inComponent: 0, animated: true)
        
    }
    
    fileprivate func updateTableviewLayout() {
        // Force layout update
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
    }
    
    fileprivate func updateEndDate() {
        
        // Defining endDate and minimumEndDate depending on beginDate and frequency
        datePickerEndDate.minimumDate = viewModel.minimumEndDateUTC
        viewModel.endDateUTC = max(datePickerEndDate.minimumDate!, viewModel.endDateUTC)
        labelEndDate.text = viewModel.endDateString
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        // Keep reference to the category that should be expanded, while other two need to be collapsed
        expandedCell = indexPath.row
        updateTableviewLayout()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        // Define currentCell and cellIdentifier
        guard let currentCell = tableView.cellForRow(at: indexPath) as? CollapsableTableViewCell, let cellIdentifier = tableView.cellForRow(at: indexPath)?.reuseIdentifier else {
            return 0
        }
        
        // Hide endDate if frequency "Once" is selected
        if (cellIdentifier == "cellScheduleEndDate") && (viewModel.onceIsSelected) {
            
            return 0
            
        } else {
            
            if indexPath.row == expandedCell {
                return currentCell.expandedHeight
            } else {
                return currentCell.collapsedHeight
            }
        }
    }
    
    // MARK: Picker view delegate
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        viewModel.frequencyID = pickerFrequency.selectedRow(inComponent: 0)
        labelFrequency.text = viewModel.frequencyText

        updateEndDate()
        updateTableviewLayout()
    }
    
    // MARK: Picker view datasource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return ScheduleFrequency.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return ScheduleFrequency(rawValue: row)?.description
    }

}
