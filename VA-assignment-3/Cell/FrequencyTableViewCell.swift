//
//  EndDateTableViewCell.swift
//  VA-assignment-3
//
//  Created by Frans Glorie on 10/02/2018.
//  Copyright © 2018 Frans Glorie. All rights reserved.
//

import UIKit

class FrequencyTableViewCell: UITableViewCell {

}

extension FrequencyTableViewCell: CollapsableTableViewCell {
    
    var isCollapsable: Bool {
        return true
    }
    
    var expandedHeight: CGFloat {
        return 180
    }
    
    var collapsedHeight: CGFloat {
        return 50
    }
    
}
