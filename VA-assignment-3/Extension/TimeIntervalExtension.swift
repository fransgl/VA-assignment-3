//
//  TimeIntervalExtension.swift
//  VA-assignment-2
//
//  Created by Frans Glorie on 08/02/2018.
//  Copyright © 2018 Frans Glorie. All rights reserved.
//

import Foundation

extension TimeInterval {
    
    static let month: TimeInterval = 30 * 24 * 3600
    static let week: TimeInterval = 7 * 24 * 3600
    static let day: TimeInterval = 24 * 3600
    static let hour: TimeInterval  = 3600
    
}
