//
//  ScheduleViewModel.swift
//  VA-assignment-2
//
//  Created by Frans Glorie on 08/02/2018.
//  Copyright © 2018 Frans Glorie. All rights reserved.
//

import Foundation

protocol ScheduleDelegate {
    func onScheduleModelUpdated()
}

class ScheduleViewModel {
    
    // Variables
    private var _model: ScheduleModel!
    
    // Functions
    init(model: ScheduleModel) {
        self._model = model
    }
    
    // Computed properties
    var beginDateString: String {
        return _model.dateBegin.toLocalDateString
    }
    
    var endDateString: String {
        return _model.dateEnd.toLocalDateString
    }
    
    var beginDateUTC: Date {
        get {
            return _model.dateBegin
        }
        set {
            _model.dateBegin = newValue
        }
    }
    
    var minimumEndDateUTC: Date {
        
        switch frequency {
        case .daily:
            return beginDateUTC.addingTimeInterval(.day)
        case .weekly:
            return beginDateUTC.addingTimeInterval(.week)
        case .monthly:
            return Calendar.current.date(byAdding: DateComponents(month: 1), to: beginDateUTC)!
        default:
            return beginDateUTC
        }
        
    }
    
    var minimumEndDateString: String {
        return minimumEndDateUTC.toLocalDateString
    }
    
    var endDateUTC: Date {
        get {
            return _model.dateEnd
        }
        set {
            _model.dateEnd = newValue
        }
    }
    
    var frequency: ScheduleFrequency {
        get {
           return _model.frequency
        }
        set {
            _model.frequency = newValue
        }
    }
    
    var frequencyText: String {
        get {
            return _model.frequency.description
        }
    }
    
    var frequencyID: Int {
        get {
            return _model.frequency.rawValue
        }
        set {
            _model.frequency = ScheduleFrequency(rawValue: newValue) ?? ScheduleFrequency.once
        }
    }
    
    var onceIsSelected: Bool {
        return _model.frequency == .once
    }
    
}
